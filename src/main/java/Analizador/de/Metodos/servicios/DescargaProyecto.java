package Analizador.de.Metodos.servicios;

import net.lingala.zip4j.ZipFile;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
@Service
public class DescargaProyecto {
        public void unzipVersions(List<File> zipFiles) throws IOException {
            for(File version:zipFiles) {
                ZipFile zipFile = new ZipFile(version.getAbsoluteFile());
                String fileName = version.getParentFile().getName()+"-"+zipFile.getFile().getName().replace(".zip","");
                //zipFile.renameFile(zipFile.getFileHeaders().get(0),fileName);
                zipFile.extractAll(version.getParentFile().getAbsolutePath());
            }
        }
    }
