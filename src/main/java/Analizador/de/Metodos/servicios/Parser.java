package Analizador.de.Metodos.servicios;

import Analizador.de.Metodos.modelo.MetodoHistoria;
import Analizador.de.Metodos.modelo.Metodo;
import Analizador.de.Metodos.modelo.MetodoVersion;

import Analizador.de.Metodos.repositorios.MetodoHistoriaRepositorio;
import Analizador.de.Metodos.repositorios.MetodoVersionRepositorio;
import com.github.javaparser.ParseProblemException;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import Analizador.de.Metodos.repositorios.MetodoRepositorio;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

@Service
@Transactional
public class Parser {
    @Autowired
    private MetodoRepositorio metodoRepositorio;
    @Autowired
    private MetodoVersionRepositorio versionRepositorio;
     @Autowired
    private MetodoHistoriaRepositorio metodoHistoriaRepositorio;

     private HashMap<String, MetodoHistoria> mapaHistoria;

    public void buscarDirectorio(String projecto) throws IOException {
        File dir = new File("src/main/resources/" + projecto);
        this.mapaHistoria=new HashMap<>();
        File[] versions = dir.listFiles(new FiltroDirectorio());
        for (File version : versions) {
            getMetodos(encontrarArchivos(version), version);
        }
        FileUtils.deleteDirectory(dir);
    }
    private List<File> encontrarArchivos(File version) {
        String[] extension = new String[]{"java"};
        List<File> files = (List<File>) FileUtils.listFiles(version, extension, true);
        return files;
    }

    private void getMetodos(List<File> clases, File version) {
        clases.stream().forEach(e -> getMetodo(e, version));

    }

    private void getMetodo(File file, File version) {
        CompilationUnit cu = null;
        String nombreClase = file.getName().replace(".java", "");
        try { cu = StaticJavaParser.parse(file);
            Optional<ClassOrInterfaceDeclaration> classOrInterfaceDeclarationOpt = cu.getClassByName(nombreClase);
            if (classOrInterfaceDeclarationOpt.isPresent()) {
                ClassOrInterfaceDeclaration classOrInterfaceDeclaration = getClassDeclaration(classOrInterfaceDeclarationOpt);
                for (int i = 0; i < classOrInterfaceDeclaration.getMethods().size(); i++) {
                    MethodDeclaration methodDeclaration= classOrInterfaceDeclaration.getMethods().get(i);
                    Metodo metodo = new Metodo();
                    MetodoVersion metodoVersion = new MetodoVersion();
                    metodo.setMetodoFirma(methodDeclaration.getDeclarationAsString());
                    metodo.setCodigoMetodo(methodDeclaration.getBody().toString());
                    metodo.setTextLines(methodDeclaration.getBody().toString().lines().count());
                    metodo.setMetricaMetodo(methodDeclaration.getParameters().size());
                    metodoRepositorio.save(metodo);
                    metodoVersion=Versiones(version, metodo);

                    if(this.mapaHistoria.containsKey(metodoVersion.getMetodo().getMetodoFirma())){
                        MetodoHistoria metodoHistoria = this.mapaHistoria.get(metodoVersion.getMetodo().getMetodoFirma());
                            metodoHistoria.Agregar(Versiones(version, metodo));
                            metodoHistoriaRepositorio.save(metodoHistoria);
                    } else{
                        MetodoHistoria metodoHistoria = new MetodoHistoria();
                        metodoHistoria.Agregar(metodoVersion);
                        mapaHistoria.put(metodoVersion.getMetodo().getMetodoFirma(), metodoHistoria);
                        metodoHistoriaRepositorio.save(metodoHistoria);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseProblemException pp) {
        }
    }
    private ClassOrInterfaceDeclaration getClassDeclaration
            (Optional<ClassOrInterfaceDeclaration> classOrInterfaceDeclaration) {
        return classOrInterfaceDeclaration.get();
    }
    private MetodoVersion Versiones(File version, Metodo metodo) {
        MetodoVersion metodoVersion = new MetodoVersion();
        metodoVersion.setVersionNombre(version.getName());
        metodoVersion.setMetodo(metodo);
        versionRepositorio.save(metodoVersion);
        return metodoVersion;
    }
}
