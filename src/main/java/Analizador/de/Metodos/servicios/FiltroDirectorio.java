package Analizador.de.Metodos.servicios;

import java.io.File;
import java.io.FileFilter;

public class FiltroDirectorio implements FileFilter {
    @Override
    public boolean accept(File file) {
        return file.isDirectory();
    }
}
