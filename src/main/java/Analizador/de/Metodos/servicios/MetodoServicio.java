package Analizador.de.Metodos.servicios;

import Analizador.de.Metodos.modelo.Metodo;
import Analizador.de.Metodos.modelo.MetodoVersion;
import Analizador.de.Metodos.repositorios.MetodoHistoriaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import Analizador.de.Metodos.repositorios.MetodoRepositorio;

import java.util.List;
import java.util.Optional;

@Service
public class MetodoServicio {

    @Autowired
    private MetodoRepositorio metodoRepositorio;
    @Autowired
    private MetodoHistoriaRepositorio metodoHistoriaRepositorio;

    public List<Metodo> getMetodo(){return this.metodoRepositorio.findAll();}

    public Metodo getMetodo(Long id){
        Optional<Metodo> optional = this.metodoRepositorio.findById(id);
        if(this.metodoRepositorio.existsById(id)) {
            return this.metodoRepositorio.findById(id).get();
        }else{
            throw new ResponseStatusException(HttpStatus.FOUND,"El metodo no existe");
        }
    }

    public Metodo updateMetodo(Long id,float rating) {
        Metodo metodo = this.getMetodo(id);
        metodo.setRating(rating);
        return this.metodoRepositorio.save(metodo);
    }
    public List<MetodoVersion> versionLista(Long id){
       return this.metodoHistoriaRepositorio.gethistoria(id).getVersiones();
    }
}
