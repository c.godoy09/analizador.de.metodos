package Analizador.de.Metodos.controlador;

import Analizador.de.Metodos.modelo.Metodo;
import Analizador.de.Metodos.servicios.MetodoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/api")
@CrossOrigin
public class MetodoControlador {
    @Autowired
    private MetodoServicio metodoServicio;

    @GetMapping("")
    public String getMetodo(Model model) {
        model.addAttribute("metodos", this.metodoServicio.getMetodo());
        return "metodoLista";
    }

    @GetMapping("/{id}")
    public String getMetodo(@PathVariable Long id, Model model) {
        model.addAttribute("metodo", this.metodoServicio.getMetodo(id));
        model.addAttribute("historia", this.metodoServicio.versionLista(id));
        return "metodo";
    }

    @PostMapping("")
    public String actualizarMetodo(@RequestParam("metodoId") Long metodoId, @RequestParam("rating") double rating) {
        this.metodoServicio.updateMetodo(metodoId, (float) rating);
        return "redirect:/api/" + metodoId + "/" + metodoId;
    }

    @PostMapping("/{id}")
    public String seleccionVersion(Metodo metodo) {
        return "redirect:/api/" + metodo.getMetodoId();
    }

    @GetMapping("/{id}/{id}")
    public String getMetod(@PathVariable Long id, Model model) {
        model.addAttribute("metodo", this.metodoServicio.getMetodo(id));
        return "metodoParametro";
    }
}
