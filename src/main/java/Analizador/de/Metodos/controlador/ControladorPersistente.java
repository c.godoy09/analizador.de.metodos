package Analizador.de.Metodos.controlador;

import Analizador.de.Metodos.servicios.DescargaProyecto;
import Analizador.de.Metodos.servicios.Parser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/persist")
@CrossOrigin
public class ControladorPersistente {
    @Autowired
    private Parser parser;

    @Autowired
    private DescargaProyecto descargaProyecto;

    @GetMapping("")
    public void proyectoPersistente(@RequestParam String nombreProyecto) throws IOException {
        List<File> files = new ArrayList<>();
        files.add(new File("src/main/resources/" + nombreProyecto + ".zip"));
        descargaProyecto.unzipVersions(files);
        parser.buscarDirectorio(nombreProyecto);
    }

}
