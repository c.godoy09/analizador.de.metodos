package Analizador.de.Metodos.modelo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.File;
import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@Entity
 public class MetodoVersion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long versionId;
    @Column
    private String versionNombre;
    @OneToOne
    private Metodo metodo;
    public MetodoVersion(String versionNombre) {
        this.versionNombre =versionNombre ;
    }
}
