package Analizador.de.Metodos.modelo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter

@Entity
public class MetodoHistoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long historiaId;

    @ManyToMany(fetch = FetchType.EAGER )
    private List<MetodoVersion> versiones;

    public MetodoHistoria() {
        this.versiones = new ArrayList<>();
    }
    public void Agregar(MetodoVersion version){
        this.versiones.add(version);
    }

}

