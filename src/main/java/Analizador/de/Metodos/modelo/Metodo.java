package Analizador.de.Metodos.modelo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@Entity
public class Metodo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long metodoId;

    @Column
    private String metodoFirma;

    @Lob
    private String codigoMetodo;

    @Column
    private Long textLines;
    @Column
    private float rating;

    @Column
    int metricaMetodo;

    public Metodo(String metodoFirma, String codigoMetodo, float rating,Long textLines) {
        this.metodoFirma =metodoFirma ;
        this.codigoMetodo = codigoMetodo;
        this.rating = rating;
        this.textLines = textLines;
    }
}
