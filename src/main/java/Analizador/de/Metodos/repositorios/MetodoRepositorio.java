package Analizador.de.Metodos.repositorios;

import Analizador.de.Metodos.modelo.Metodo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface MetodoRepositorio extends JpaRepository<Metodo,Long> {
}
