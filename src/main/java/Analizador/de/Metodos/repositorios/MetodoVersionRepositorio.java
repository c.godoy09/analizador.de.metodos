package Analizador.de.Metodos.repositorios;


import Analizador.de.Metodos.modelo.MetodoVersion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface MetodoVersionRepositorio extends JpaRepository<MetodoVersion,Long> {
}
