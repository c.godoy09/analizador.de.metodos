package Analizador.de.Metodos.repositorios;

import Analizador.de.Metodos.modelo.MetodoHistoria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public interface MetodoHistoriaRepositorio extends JpaRepository<MetodoHistoria,Long> {
    @Query(value = "select * from metodo_historia a where historia_id in(select metodo_historia_historia_id from metodo_historia_versiones j where versiones_version_id in (select version_id from metodo_version where metodo_metodo_id = :id ))",nativeQuery=true)
    public MetodoHistoria gethistoria(@Param("id") Long id );
}
