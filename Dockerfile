# (1) use Alpine Linux for build stage
FROM alpine:3.11.5 as build
# (2) install build dependencies
RUN apk --no-cache add openjdk11-jdk openjdk11-jmods
RUN apk --no-cache add maven

# build JDK with less modules
RUN /usr/lib/jvm/default-jvm/bin/jlink \
    --compress=2 \
    --module-path /usr/lib/jvm/default-jvm/jmods \
    --add-modules java.base,java.logging,java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument \
    --output /jdk-minimal

# fetch maven dependencies
WORKDIR /build
ADD pom.xml /build
ADD . /build
RUN mvn -DskipTests  package -B
RUN cp /build/target/*.jar /app.jar
EXPOSE 8080
CMD /jdk-minimal/bin/java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar